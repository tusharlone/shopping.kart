﻿$(document).ready(function () {
    $('.multiselect').multiselect({
        nonSelectedText: '-- Select --',
        includeSelectAllOption: true,
        maxHeight: 200,
        buttonWidth: '100%'
    });
    RefreshDatePicker();
});

function RefreshDatePicker() {
    $('.datepicker').datepicker({
        format: "dd-M-yyyy"
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
}

function setMinDate(Selector, newDate) {
    $(Selector).datepicker('setStartDate', newDate);
}

function setMaxDate(Selector, newDate) {
    $(Selector).datepicker('setEndDate', newDate);
}

function GetSaveData(FormSelector, IsFileUploadRequired) {
    if (IsFileUploadRequired) {
        return new FormData($(FormSelector)[0]);
    }
    else {
        return $(FormSelector).serialize();
    }
}

function CallAjaxMethod(StrPriURL, ObjPriData, IsFileUpdateRequired) {

    if (IsFileUpdateRequired) {
        return $.ajax({
            url: StrPriURL,
            type: 'POST',
            contentType: false,
            processData: false,
            data: ObjPriData
        }).then(function (e) {
            return e;
        }).always(function (e) {
        });
    }
    else {
        return $.post(StrPriURL, ObjPriData).then(function (r) {
            return r;
        }).always(function (e) {
        });
    }
}

function InitializeForm(FormSelector, DefaultButtonSelector, Fn1, Fn2) {
    if (DefaultButtonSelector == undefined || DefaultButtonSelector == null || DefaultButtonSelector == "") {
        DefaultButtonSelector = ".DefaultButton";
    }

    var EnterHandler = function (event) {
        if (event.keyCode == 13) {
            $(DefaultButtonSelector + ':first').click();
        }
    };

    RefreshFormValidation(FormSelector, Fn1, Fn2);
    $(FormSelector).find('input:not(.datepicker),select').filter(':visible:first').focus();
    $(FormSelector).find('input,select').unbind('keydown', EnterHandler);
    $(FormSelector).find('input,select').bind('keydown', EnterHandler);
    $(FormSelector).on("blur", 'input[type="text"],textarea', TrimTextboxes);

    $(FormSelector).submit(function () { return false; });
}

function RefreshFormValidation(FormSelector, Fn1, Fn2) {
    RefreshValidation(FormSelector);
    FormatFormValidation(FormSelector, Fn1, Fn2);
}

function TrimTextboxes() {
    $(event.target).val($(event.target).val().trim());
}

function RefreshValidation(FormSelector) {
    $(FormSelector).removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
    $.validator.unobtrusive.parse($(FormSelector));//add the form validation
}

function FormatFormValidation(FormSelector, OnSuccess, OnFailure) {
    var form = $(FormSelector);
    var formData = $.data(form[0]);
    if (formData.validator != undefined) {
        var settings = formData.validator.settings;
        var oldErrorPlacement = settings.errorPlacement;
        var oldSuccess = settings.success;
        settings.errorPlacement = function (label, element) {
            oldErrorPlacement(label, element);
            label.parents('.form-group').addClass('has-error');
            label.addClass('text-danger');
            if (OnFailure != undefined) {
                OnFailure();
            }
        };
        settings.success = function (label) {
            label.parents('.form-group').removeClass('has-error');
            oldSuccess(label);
            if (OnSuccess != undefined) {
                OnSuccess();
            }
        };
    }
}

function alert(message, header) {
    if (header == null || header == "" || header == undefined) {
        $('#AlertHeader').text("Alert");
    }
    else {
        $('#AlertHeader').text(header);
    }

    $('#AlertTextId').text(message);
    $("#alert-box").modal("show");
}

function confirm(message) {
    $('#ConfirmationText').text(message);
    $('#confirmation-box').modal('show');
    $('#confirmation-box').modal({ backdrop: 'static', keyboard: false });
}

function ConfirmBoxYesClickHandler(SuccessMethod) {
    $('#btnYes').unbind();
    $('#btnYes').click(function () {
        SuccessMethod();
    });
}

function ConfirmBoxNoClickHandler(FailuerMethod) {
    $('#btnNo,.close').unbind();
    $('#btnNo,.close').click(function () {
        FailuerMethod();
    });
}