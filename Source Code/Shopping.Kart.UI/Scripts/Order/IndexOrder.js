﻿$(document).ready(function () {
    EventHandlers();
});

function EventHandlers() {
    $("#MainDivSection").on("click", ".Show-Details", ViewImagesClickHandler);
    $("#MainDivSection").on("click", "#btnAddToCart", AddToCartClickHandler);
}

function ViewImagesClickHandler(event) {
    event.preventDefault();
    var prodId = $(this).parents('div.product-cart').attr('data-id');
    var url = "/Order/ViewProductDetals?id=" + prodId;

    CallAjaxMethod(url).then(function (r) {
        $("#DetailsModal").html(r);
        $("#myModal").on("shown.bs.modal", function () {
            $(".modal-backdrop.in").remove();
        });

        $("#myModal").modal("show");
        showSlides(slideIndex);
        openModal(); currentSlide(1);
    });
}

function AddToCartClickHandler(event) {
    event.preventDefault();
    var parentDiv = $(this).parents('div.product-cart');
    var prodId = parentDiv.attr('data-id');
    var prodPrice = parentDiv.attr('data-price').replace(',', '');

    CallAjaxMethod("/Order/AddToCart", { id: prodId, price: prodPrice }).then(function (r) {
        if (r.success) {
            alert(r.message, "Success");
        }
        else {
            alert(r.message, "Failure");
        }
    });
}