﻿$(document).ready(function () {
    EventHandlers();
});

function EventHandlers() {
    $('#tblCartProduct').on('keyup', '.Quantity[type="text"]', QuantityChangeHandler);
}

function QuantityChangeHandler() {

    var parentRow = $(this).parents('tr[data-id]');
    var price = parentRow.attr('data-price');
    var quantity = $(this).val();

    var totalProductCost = (price * quantity).toFixed(2);

    parentRow.find('.totalAmount').text(totalProductCost);
}