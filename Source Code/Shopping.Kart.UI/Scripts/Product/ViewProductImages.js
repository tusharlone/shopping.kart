﻿$(document).ready(function () {

});

function openModal() {
    $("#myModal").css('display', 'block');
}

function closeModal() {
    $("#myModal").css('display', 'none');
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var slides = $(".mySlides");
    var dots = $(".demo");
    if (n > slides.length) { slideIndex = 1; }
    if (n < 1) { slideIndex = slides.length; }
    $(".mySlides").css('display', 'none');
    $(".demo").removeClass('active');
    slides.eq(slideIndex - 1).css('display', 'block');
    dots.eq(slideIndex - 1).addClass('active');
}
