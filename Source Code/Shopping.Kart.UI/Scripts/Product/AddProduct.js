﻿$(document).ready(function () {
    EventHandlers();
    InitializeForm('#FrmAdd', '#BtnSave');
    setMaxDate('#txtManufacturingDate', new Date());
});

function EventHandlers() {
    $('#FrmAdd').on('click', '.addRow', AddRowClickHandler);
    $('#FrmAdd').on('click', '#BtnSave', SaveClickHandler);
}

function AddRowClickHandler() {
    var newRow = $(this).parents('tr').clone();
    newRow.find('input[type="text"],input[type="checkbox"],input[type="file"],textarea').prop('checked', false).val("");
    newRow.appendTo($(this).parents('tbody'));

    $(this).removeClass('btn-primary addRow').addClass('btn-warning removeRow').val('Remove');

    $('#FrmAdd').on('click', '.removeRow', RemoveRowClickHandler);
}

function RemoveRowClickHandler() {
    $(this).parents('tr').remove();
}

var productImages = [];



function SaveClickHandler() {
    event.preventDefault();
    var prodSpec = [];

    $('#tblProdSpec tbody tr').each(function () {
        prodSpec.push({ Description: $(this).find('#txtDiscription').val(), Value: $(this).find('#txtValue').val() });
    });

    var prodImages = [];

    $('#tblProdImg tbody tr').each(function (i) {
        prodImages.push({
            //ProductImage: $(this).find('#txtProductImage')[0].files,
            IsPrimaryImage: $(this).find('#chkIsPrimaryImage').is(":checked")
        });
    });

    var data = {
        ProductName: $('#txtProductName').val(),
        ProductCode: $('#txtProductCode').val(),
        SelectedBrandId: $('#ddlBrands option:selected').val(),
        SelectedCategories: $('#ddlCategories').val(),
        ProductSpecifications: prodSpec,
        ProductImages: prodImages,
        ProductColor: $('#txtProductColor').val(),
        ProductPrice: $('#txtProductPrice').val(),
        ManufacturingDate: $('#txtManufacturingDate').val()
    };

    CallAjaxMethod("/Product/Save", data).then(function (r) {
        if (r.success) {
            $("#alert-box").on("hidden.bs.modal", function () {
                window.location.href = "/Product/Index";
            });
            alert(r.message, "Success");
        }
        else {
            alert(r.message, "Failure");
        }
    });
}