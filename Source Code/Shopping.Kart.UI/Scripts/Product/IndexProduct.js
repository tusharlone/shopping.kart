﻿$(document).ready(function () {
    InitializeForm('#FrmSearch', '#btnSearch');
    $('.product-name,.brand-name').tooltip();
    EventHandlers();
    setMaxDate('#txtDateFrom', new Date());
    setMaxDate('#txtDateTo', new Date());
});

function EventHandlers() {
    $("#MainDivSection").on("click", "#lnkShowImages", ViewImagesClickHandler);
    $('#MainDivSection').on("change", "#txtDateFrom", FromDateChangeHandler);
}

function ViewImagesClickHandler() {
    event.preventDefault();
    var url = $(this).attr('href');

    CallAjaxMethod(url).then(function (r) {
        $("#ImageModal").html(r);
        $("#myModal").on("shown.bs.modal", function () {
            $(".modal-backdrop.in").remove();
        });

        $("#myModal").modal("show");
        showSlides(slideIndex);
    });
}

function FromDateChangeHandler() {
    if ($('#txtDateFrom').val() > $('#txtDateTo').val()) {
        $('#txtDateTo').datepicker('update', '');
        setMinDate('#txtDateTo', $('#txtDateFrom').val());
    }
    else {
        setMinDate('#txtDateTo', $('#txtDateFrom').val());
    }
}