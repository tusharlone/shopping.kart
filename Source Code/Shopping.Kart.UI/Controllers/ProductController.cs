﻿using Shopping.Kart.BAL.BusinessLogics;
using Shopping.Kart.BAL.CustomEntities;
using Shopping.Kart.BAL.DataContext;
using Shopping.Kart.UI.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shopping.Kart.UI.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Product
        #region Public
        public ActionResult Index()
        {
            ProductBAL productBAL = new ProductBAL();
            ProductIndexVM model = new ProductIndexVM
            {
                SearchParameter = new ProductSearchVM
                {
                    Brands = GetListItems(productBAL.GetBrandsForDropDown()),
                    Categories = GetListItems(productBAL.GetCategoriesForDropDown())
                },
                ProductDetails = SearchProducts()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            ProductBAL productBAL = new ProductBAL();
            ProductAddVM model = new ProductAddVM
            {
                Brands = GetListItems(productBAL.GetBrandsForDropDown()),
                Categories = GetListItems(productBAL.GetCategoriesForDropDown())
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(ProductAddVM model)
        {
            model.ProductImages = UploadProductImages(model.ProductImages);

            bool isProductSaved = new ProductBAL().SaveProduct(MapProductAddModelToProductDto(model));

            return Json(new { success = isProductSaved, message = isProductSaved ? "Product Saved Successfully." : "Error while saving data." });
        }

        [HttpPost]
        public ActionResult ViewProductImages(int id)
        {
            return PartialView("_ProductImageModal");
        }
        #endregion

        #region Private
        private List<ProductGridVM> SearchProducts()
        {
            return new ProductBAL().GetProductEntities().Select(x => new ProductGridVM
            {
                ProductID = x.ProductID,
                ProductName = x.ProductName,
                ProductCode = x.ProductCode,
                BrandName = x.BrandName,
                ProductCategories = x.ProductCategories,
                ProductColor = x.ProductColor,
                ProductPrice = x.ProductPrice,
                //ProductImages = x.ProductImages.Select(y => new ProductImageVM
                //{
                //    ProductImagePath = y.Item1,
                //    IsPrimaryImage = y.Item2
                //}).ToList()
            }).ToList();
        }

        private List<ProductImageVM> UploadProductImages(List<ProductImageVM> productImages)
        {
            foreach (var productImage in productImages)
            {
                try
                {
                    string fileName = GetFormattedFileName(Path.GetFileNameWithoutExtension(productImage.ProductImage.FileName), Path.GetExtension(productImage.ProductImage.FileName));
                    Directory.CreateDirectory(Server.MapPath(WebConfigReader.ProductImageUploadPath));
                    string path = Path.Combine(Server.MapPath(WebConfigReader.ProductImageUploadPath), fileName);

                    productImage.ProductImage.SaveAs(path);

                    productImage.ProductImagePath = path;
                }
                catch (Exception)
                {
                    productImage.ProductImagePath = string.Empty;
                }
            }

            return productImages;
        }

        private Mst_Product MapProductAddModelToProductDto(ProductAddVM model)
        {
            return new Mst_Product
            {
                ProductName = model.ProductName,
                ProductCode = model.ProductCode,
                BrandID = model.SelectedBrandId,
                ProductColor = model.ProductColor,
                ProductPrice = decimal.Parse(model.ProductPrice),
                ManufacturingDate = DateTime.Parse(model.ManufacturingDate),
                IsActive = true,
                CreatedOn = DateTime.UtcNow,
                Map_ProductCategory = model.SelectedCategories.Select(x => new Map_ProductCategory
                {
                    CategoryID = x
                }).ToList(),
                Map_ProductSpecification = model.ProductSpecifications.Select(x => new Map_ProductSpecification
                {
                    Description = x.Description,
                    Value = x.Value
                }).ToList(),
                Map_ProductImage = model.ProductImages.Where(x => !string.IsNullOrEmpty(x.ProductImagePath)).Select(x => new Map_ProductImage
                {
                    ImagePath = x.ProductImagePath,
                    IsPrimaryImage = x.IsPrimaryImage
                }).ToList()
            };
        }

        private List<SelectListItem> GetListItems(List<DropDownItems> list)
        {
            return list.Select(x => new SelectListItem
            {
                Value = x.Id,
                Text = x.Text
            }).ToList();
        }
        #endregion
    }
}