﻿using Shopping.Kart.BAL.BusinessLogics;
using Shopping.Kart.UI.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shopping.Kart.UI.Controllers
{
    public class OrderController : BaseController
    {
        // GET: Order
        public ActionResult Index()
        {
            return View(new OrderIndexVM { ProductDetails = GetProducts() });
        }

        [HttpPost]
        public ActionResult ViewProductDetals(int id)
        {
            return PartialView("_ProductDetailModal", GetProducts().Where(x => x.ProductID == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult AddToCart(int id, decimal price)
        {
            var result = new OrderBAL().AddProductToCart(id, price);

            return Json(new { success = result, message = result ? "Product Cart Successfully." : "Error while Add To Cart." });
        }

        public ActionResult CheckOut()
        {
            return View(new CartProductVM
            {
                CartProducts = new OrderBAL().GetCartProducts()
            });
        }

        private List<ProductDetailVM> GetProducts()
        {
            return new ProductBAL().GetProductEntities().Select(x => new ProductDetailVM
            {
                ProductID = x.ProductID,
                ProductName = x.ProductName,
                ProductCode = x.ProductCode,
                BrandName = x.BrandName,
                ProductCategories = x.ProductCategories,
                ProductColor = x.ProductColor,
                ProductPrice = x.ProductPrice,
                //ProductImages = x.ProductImages.Select(y => new ProductImageVM
                //{
                //    ProductImagePath = y.Item1,
                //    IsPrimaryImage = y.Item2
                //}).ToList()
            }).ToList();
        }
    }
}