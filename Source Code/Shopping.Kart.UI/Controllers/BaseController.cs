﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shopping.Kart.UI.Controllers
{
    public class BaseController : Controller
    {
        public string GetFormattedFileName(string title, string extension)
        {
            DateTime CurrentDateTime = DateTime.UtcNow;
            return title + "_" + CurrentDateTime.Day + "_" + CurrentDateTime.Month + "_" + CurrentDateTime.Year + "_" + CurrentDateTime.TimeOfDay.Hours + "_" + CurrentDateTime.TimeOfDay.Minutes + "_" + CurrentDateTime.TimeOfDay.Milliseconds + extension;
        }
    }
}