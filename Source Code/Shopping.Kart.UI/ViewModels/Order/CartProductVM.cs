﻿using Shopping.Kart.BAL.CustomEntities.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Order
{
    public class CartProductVM : BaseVM
    {
        public List<ProductEntity> CartProducts { get; set; }
    }
}