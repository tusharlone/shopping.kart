﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Order
{
    public class OrderIndexVM : BaseVM
    {
        public List<ProductDetailVM> ProductDetails { get; set; }
    }
}