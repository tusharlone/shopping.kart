﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductGridVM
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string BrandName { get; set; }
        public string ProductCategories { get; set; }
        public string ProductColor { get; set; }
        public string ProductPrice { get; set; }
        public List<ProductImageVM> ProductImages { get; set; }
    }
}