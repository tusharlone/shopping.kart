﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductIndexVM : BaseVM
    {
        public ProductSearchVM SearchParameter { get; set; }
        public List<ProductGridVM> ProductDetails { get; set; }
    }
}