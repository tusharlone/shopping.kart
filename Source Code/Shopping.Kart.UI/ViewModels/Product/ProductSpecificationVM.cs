﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductSpecificationVM
    {
        public string Description { get; set; }
        public string Value { get; set; }
    }
}