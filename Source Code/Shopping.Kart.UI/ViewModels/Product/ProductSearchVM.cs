﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductSearchVM
    {
        public string ProductNameOrCode { get; set; }
        public int SelectedBrandId { get; set; }
        public List<SelectListItem> Brands { get; set; }
        public List<int> SelectedCategories { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public bool IsActive { get; set; }
        public string EarnFrom { get; set; }
        public string EarnTo { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}