﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductImageVM
    {
        public HttpPostedFileBase ProductImage { get; set; }
        public string ProductImagePath { get; set; }
        public bool IsPrimaryImage { get; set; }
    }
}