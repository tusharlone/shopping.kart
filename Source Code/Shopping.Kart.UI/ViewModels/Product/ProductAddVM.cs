﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shopping.Kart.UI.ViewModels.Product
{
    public class ProductAddVM : BaseVM
    {
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public int SelectedBrandId { get; set; }
        public List<SelectListItem> Brands { get; set; }
        public List<int> SelectedCategories { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public ProductSpecificationVM ProductSpecification { get; set; }
        public List<ProductSpecificationVM> ProductSpecifications { get; set; }
        public ProductImageVM ProductImage { get; set; }
        public List<ProductImageVM> ProductImages { get; set; }
        public string ProductColor { get; set; }
        public string ProductPrice { get; set; }
        public string ManufacturingDate { get; set; }
    }
}