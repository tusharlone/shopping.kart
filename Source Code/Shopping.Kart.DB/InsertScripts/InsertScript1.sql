﻿USE [Shopping.Kart]
GO
SET IDENTITY_INSERT [dbo].[Dev_Brand] ON 

INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (1, N'Maggi', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (2, N'Cadbary', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (3, N'Sony', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (4, N'Sunfeast', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (5, N'Xiaomi', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (6, N'LG', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (7, N'Samsung', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (8, N'Woodland', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (10, N'Nike', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (11, N'Adidas', 1)
INSERT [dbo].[Dev_Brand] ([BrandID], [BrandName], [IsActive]) VALUES (12, N'Puma', 1)
SET IDENTITY_INSERT [dbo].[Dev_Brand] OFF
SET IDENTITY_INSERT [dbo].[Dev_Category] ON 

INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (1, N'Noodle', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (2, N'Pasta', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (3, N'Masala', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (4, N'Chocolate', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (5, N'Silk', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (6, N'Biscuit', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (7, N'Bytes', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (8, N'Milk', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (9, N'Headphones', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (10, N'MP3 Players', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (11, N'Wireless Speakers', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (12, N'Mobile', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (13, N'Home Theatre', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (14, N'Cameras', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (15, N'PlayStation', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (16, N'TV', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (17, N'Watch', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (18, N'Fitness Bands', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (19, N'Treamer', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (20, N'Shoes', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (21, N'Bags', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (22, N'Shirt', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (23, N'T-Shirt', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (24, N'Jeans', 1)
INSERT [dbo].[Dev_Category] ([CategoryID], [Description], [IsActive]) VALUES (25, N'Cap', 1)
SET IDENTITY_INSERT [dbo].[Dev_Category] OFF
