﻿CREATE TABLE [dbo].[Map_OrderProductCart] (
    [ProductCartID]    INT          IDENTITY (1, 1) NOT NULL,
    [ProductID]        INT          NOT NULL,
    [ProductCartPrice] DECIMAL (18) NOT NULL,
    [ProductCartOn]    DATETIME     NOT NULL,
    [IsActive]         BIT          NOT NULL,
    CONSTRAINT [PK_Map_OrderProductCart] PRIMARY KEY CLUSTERED ([ProductCartID] ASC),
    CONSTRAINT [FK_Map_OrderProductCart_Mst_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Mst_Product] ([ProductID])
);

