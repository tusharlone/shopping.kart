﻿CREATE TABLE [dbo].[Dev_Category] (
    [CategoryID]  INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [IsActive]    BIT          NOT NULL,
    CONSTRAINT [PK_Dev_Category] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);

