﻿CREATE TABLE [dbo].[Map_ProductImage] (
    [ProductImageID] INT           IDENTITY (1, 1) NOT NULL,
    [ProductID]      INT           NOT NULL,
    [ImagePath]      VARCHAR (500) NOT NULL,
    [IsPrimaryImage] BIT           NOT NULL,
    CONSTRAINT [PK_Map_ProductImage] PRIMARY KEY CLUSTERED ([ProductImageID] ASC),
    CONSTRAINT [FK_Map_ProductImage_Mst_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Mst_Product] ([ProductID])
);



