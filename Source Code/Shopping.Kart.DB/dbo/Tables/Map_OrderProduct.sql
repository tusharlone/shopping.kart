﻿CREATE TABLE [dbo].[Map_OrderProduct] (
    [OrderProductID] INT IDENTITY (1, 1) NOT NULL,
    [OrderID]        INT NOT NULL,
    [ProductCartID]  INT NOT NULL,
    [Quantity]       INT NOT NULL,
    CONSTRAINT [PK_Map_OrderProduct] PRIMARY KEY CLUSTERED ([OrderProductID] ASC),
    CONSTRAINT [FK_Map_OrderProduct_Map_OrderProductCart] FOREIGN KEY ([ProductCartID]) REFERENCES [dbo].[Map_OrderProductCart] ([ProductCartID]),
    CONSTRAINT [FK_Map_OrderProduct_Mst_Order] FOREIGN KEY ([OrderID]) REFERENCES [dbo].[Mst_Order] ([OrderID])
);





