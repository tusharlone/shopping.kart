﻿CREATE TABLE [dbo].[Dev_Brand] (
    [BrandID]   INT          IDENTITY (1, 1) NOT NULL,
    [BrandName] VARCHAR (50) NOT NULL,
    [IsActive]  BIT          NOT NULL,
    CONSTRAINT [PK_Dev_Brand] PRIMARY KEY CLUSTERED ([BrandID] ASC)
);

