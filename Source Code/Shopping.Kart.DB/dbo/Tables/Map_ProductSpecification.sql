﻿CREATE TABLE [dbo].[Map_ProductSpecification] (
    [ProductSpecID] INT           IDENTITY (1, 1) NOT NULL,
    [ProductID]     INT           NOT NULL,
    [Description]   VARCHAR (500) NOT NULL,
    [Value]         VARCHAR (500) NOT NULL,
    CONSTRAINT [PK_Map_ProductSpecification] PRIMARY KEY CLUSTERED ([ProductSpecID] ASC),
    CONSTRAINT [FK_Map_ProductSpecification_Mst_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Mst_Product] ([ProductID])
);



