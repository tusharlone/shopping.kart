﻿CREATE TABLE [dbo].[Mst_Order] (
    [OrderID]       INT      IDENTITY (1, 1) NOT NULL,
    [OrderDate]     DATETIME NOT NULL,
    [OrderStatusID] INT      NOT NULL,
    CONSTRAINT [PK_Mst_Order] PRIMARY KEY CLUSTERED ([OrderID] ASC),
    CONSTRAINT [FK_Mst_Order_Dev_OrderStatus] FOREIGN KEY ([OrderStatusID]) REFERENCES [dbo].[Dev_OrderStatus] ([OrderStatusID])
);

