﻿CREATE TABLE [dbo].[Dev_OrderStatus] (
    [OrderStatusID] INT          IDENTITY (1, 1) NOT NULL,
    [Description]   VARCHAR (50) NOT NULL,
    [IsActive]      BIT          NOT NULL,
    CONSTRAINT [PK_Dev_OrderStatus] PRIMARY KEY CLUSTERED ([OrderStatusID] ASC)
);

