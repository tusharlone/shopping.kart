﻿CREATE TABLE [dbo].[Mst_Product] (
    [ProductID]         INT          IDENTITY (1, 1) NOT NULL,
    [ProductName]       VARCHAR (50) NOT NULL,
    [ProductCode]       VARCHAR (12) NOT NULL,
    [BrandID]           INT          NOT NULL,
    [ProductColor]      VARCHAR (50) NOT NULL,
    [ProductPrice]      DECIMAL (18) NOT NULL,
    [ManufacturingDate] DATETIME     NOT NULL,
    [IsActive]          BIT          NOT NULL,
    [CreatedOn]         DATETIME     NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [FK_Mst_Product_Dev_Brand] FOREIGN KEY ([BrandID]) REFERENCES [dbo].[Dev_Brand] ([BrandID])
);

