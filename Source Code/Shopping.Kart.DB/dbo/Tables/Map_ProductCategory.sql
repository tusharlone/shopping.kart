﻿CREATE TABLE [dbo].[Map_ProductCategory] (
    [ProductCategoryID] INT IDENTITY (1, 1) NOT NULL,
    [ProductID]         INT NOT NULL,
    [CategoryID]        INT NOT NULL,
    CONSTRAINT [PK_Map_ProductCategory] PRIMARY KEY CLUSTERED ([ProductCategoryID] ASC),
    CONSTRAINT [FK_Map_ProductCategory_Dev_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Dev_Category] ([CategoryID]),
    CONSTRAINT [FK_Map_ProductCategory_Mst_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Mst_Product] ([ProductID])
);



