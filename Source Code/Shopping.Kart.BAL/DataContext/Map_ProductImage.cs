//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Shopping.Kart.BAL.DataContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class Map_ProductImage
    {
        public int ProductImageID { get; set; }
        public int ProductID { get; set; }
        public string ImagePath { get; set; }
        public bool IsPrimaryImage { get; set; }
    
        public virtual Mst_Product Mst_Product { get; set; }
    }
}
