﻿using Shopping.Kart.BAL.CustomEntities;
using Shopping.Kart.BAL.CustomEntities.Product;
using Shopping.Kart.BAL.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Shopping.Kart.BAL.BusinessLogics
{
    public class ProductBAL : BaseBAL
    {
        public List<DropDownItems> GetBrandsForDropDown()
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                return context.Dev_Brand.Where(x => x.IsActive).Select(x => new DropDownItems
                {
                    Id = x.BrandID.ToString(),
                    Text = x.BrandName
                }).ToList();
            };
        }

        public List<DropDownItems> GetCategoriesForDropDown()
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                return context.Dev_Category.Where(x => x.IsActive).Select(x => new DropDownItems
                {
                    Id = x.CategoryID.ToString(),
                    Text = x.Description
                }).ToList();
            };
        }

        public bool SaveProduct(Mst_Product product)
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                context.Mst_Product.Add(product);
                return context.SaveChanges() > 0;
            }
        }

        public List<Mst_Product> GetProductDetails()
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                return context.Mst_Product.Include(x => x.Dev_Brand)
                                          .Include(x => x.Map_ProductCategory)
                                          .Include(x => x.Map_ProductImage)
                                          .Include(x => x.Map_ProductSpecification)
                                          .ToList();
            }
        }

        public List<ProductEntity> GetProductEntities()
        {
            return GetProductDetails().Where(x => x.IsActive)
                                      .Select(x => new ProductEntity
                                      {
                                          ProductID = x.ProductID,
                                          ProductName = x.ProductName,
                                          ProductCode = x.ProductCode,
                                          BrandName = x.Dev_Brand.BrandName,
                                          ProductCategories = GetCommaSeperatedProductCategories(x.ProductID),
                                          ProductColor = x.ProductColor,
                                          ProductPrice = x.ProductPrice.ToString("N2"),
                                          //ProductImages = x.Map_ProductImage.Select(y => new Tuple<string, bool>(y.ImagePath, y.IsPrimaryImage)).ToList()
                                      }).ToList();
        }

        private string GetCommaSeperatedProductCategories(int prodId)
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                List<string> categories = context.Map_ProductCategory.Include(x => x.Dev_Category).Where(x => x.ProductID == prodId).Select(x => x.Dev_Category.Description).ToList();
                return string.Join(", ", categories);
            }
        }
    }
}
