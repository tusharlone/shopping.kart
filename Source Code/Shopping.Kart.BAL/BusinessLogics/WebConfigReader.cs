﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping.Kart.BAL.BusinessLogics
{
    public class WebConfigReader
    {
        public static string ProductImageUploadPath
        {
            get { return ConfigurationManager.AppSettings["ProductImageUploadPath"].ToString(); }
        }
    }
}
