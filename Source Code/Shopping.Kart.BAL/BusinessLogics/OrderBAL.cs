﻿using Shopping.Kart.BAL.CustomEntities.Product;
using Shopping.Kart.BAL.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Shopping.Kart.BAL.BusinessLogics
{
    public class OrderBAL : BaseBAL
    {
        public bool AddProductToCart(int prodId, decimal currentPrice)
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                context.Map_OrderProductCart.Add(new Map_OrderProductCart()
                {
                    ProductID = prodId,
                    ProductCartPrice = currentPrice,
                    ProductCartOn = DateTime.UtcNow,
                    IsActive = true
                });

                return context.SaveChanges() > 0;
            }
        }

        public List<ProductEntity> GetCartProducts()
        {
            using (ShoppingKartEntities context = new ShoppingKartEntities())
            {
                return context.Map_OrderProductCart.Include(x => x.Mst_Product.Dev_Brand).Select(x => new ProductEntity
                {
                    CartId = x.ProductCartID,
                    ProductID = x.ProductID,
                    ProductName = x.Mst_Product.ProductName,
                    ProductCode = x.Mst_Product.ProductCode,
                    BrandName = x.Mst_Product.Dev_Brand.BrandName,
                    ProductColor = x.Mst_Product.ProductColor,
                    CartPrice = x.ProductCartPrice
                }).ToList();
            }
        }
    }
}
