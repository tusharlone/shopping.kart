﻿namespace Shopping.Kart.BAL.CustomEntities
{
    public class DropDownItems
    {
        public bool Selected { get; set; }
        
        public string Text { get; set; }
        
        public string Id { get; set; }
    }
}
