﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping.Kart.BAL.CustomEntities.Product
{
    public class ProductEntity
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string BrandName { get; set; }
        public string ProductCategories { get; set; }
        public string ProductColor { get; set; }
        public string ProductPrice { get; set; }
        //public List<Tuple<string, bool>> ProductImages { get; set; }

        public int CartId { get; set; }
        public decimal CartPrice { get; set; }
    }
}
