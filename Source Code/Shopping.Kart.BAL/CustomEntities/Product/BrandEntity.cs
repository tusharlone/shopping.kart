﻿namespace Shopping.Kart.BAL.CustomEntities.Product
{
    public class BrandEntity
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public bool IsActive { get; set; }
    }
}
