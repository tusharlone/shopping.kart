﻿namespace Shopping.Kart.BAL.CustomEntities.Product
{
    public class CategoryEntity
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool IsActive { get; set; }
    }
}
